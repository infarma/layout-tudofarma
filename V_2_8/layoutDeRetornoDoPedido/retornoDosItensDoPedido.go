package layoutDeRetornoDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type RetornoDosItensDoPedido struct {
	TipoDoRegistro        int64  	`json:"TipoDoRegistro"`
	CodigoDoProduto       int64  	`json:"CodigoDoProduto"`
	NumeroDoPedido        string 	`json:"NumeroDoPedido"`
	CondicaoDePagamento   string 	`json:"CondicaoDePagamento"`
	QuantidadeAtendida    int64  	`json:"QuantidadeAtendida"`
	DescontoAplicado      float64	`json:"DescontoAplicado"`
	PrazoConcedido        int64  	`json:"PrazoConcedido"`
	QuantidadeNaoAtendida int64  	`json:"QuantidadeNaoAtendida"`
	CodigoDoMotivo        int64  	`json:"CodigoDoMotivo"`
	DescricaoDoMotivo     string 	`json:"DescricaoDoMotivo"`
}

func (r *RetornoDosItensDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRetornoDosItensDoPedido

	err = posicaoParaValor.ReturnByType(&r.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CondicaoDePagamento, "CondicaoDePagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescontoAplicado, "DescontoAplicado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.PrazoConcedido, "PrazoConcedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeNaoAtendida, "QuantidadeNaoAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoDoMotivo, "CodigoDoMotivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescricaoDoMotivo, "DescricaoDoMotivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRetornoDosItensDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"CodigoDoProduto":                      {2, 15, 0},
	"NumeroDoPedido":                      {15, 27, 0},
	"CondicaoDePagamento":                      {27, 28, 0},
	"QuantidadeAtendida":                      {28, 33, 0},
	"DescontoAplicado":                      {33, 41, 5},
	"PrazoConcedido":                      {41, 44, 0},
	"QuantidadeNaoAtendida":                      {44, 49, 0},
	"CodigoDoMotivo":                      {49, 51, 0},
	"DescricaoDoMotivo":                      {51, 101, 0},
}