package layoutDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CabecalhoDoPedido struct {
	TipoDoRegistro             int64 	`json:"TipoDoRegistro"`
	CodigoDoCliente            int64 	`json:"CodigoDoCliente"`
	NumeroDoPedido             string	`json:"NumeroDoPedido"`
	DataDoPedido               int64 	`json:"DataDoPedido"`
	TipoDaCompra               string	`json:"TipoDaCompra"`
	TipoDoRetorno              string	`json:"TipoDoRetorno"`
	ApontadorCondicaoComercial int64 	`json:"ApontadorCondicaoComercial"`
	NumeroPedidoDoCliente      string	`json:"NumeroPedidoDoCliente"`
}

func (c *CabecalhoDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalhoDoPedido

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoDoCliente, "CodigoDoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataDoPedido, "DataDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDaCompra, "TipoDaCompra")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.TipoDoRetorno, "TipoDoRetorno")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ApontadorCondicaoComercial, "ApontadorCondicaoComercial")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedidoDoCliente, "NumeroPedidoDoCliente")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalhoDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"CodigoDoCliente":                      {2, 17, 0},
	"NumeroDoPedido":                      {17, 29, 0},
	"DataDoPedido":                      {29, 37, 0},
	"TipoDaCompra":                      {37, 38, 0},
	"TipoDoRetorno":                      {38, 39, 0},
	"ApontadorCondicaoComercial":                      {39, 44, 0},
	"NumeroPedidoDoCliente":                      {44, 59, 0},
}