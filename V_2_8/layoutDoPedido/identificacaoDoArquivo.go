package layoutDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type IdentificacaoDoArquivo struct {
	TipoDoRegistro         int64 	`json:"TipoDoRegistro"`
	IdentificadorDoArquivo string	`json:"IdentificadorDoArquivo"`
	CnpjDoDitribuidor      int64 	`json:"CnpjDoDitribuidor"`
	DataDoProcessamento    int64 	`json:"DataDoProcessamento"`
	HoraDoProcessamento    int64 	`json:"HoraDoProcessamento"`
	CnpjDaIndustria        int64 	`json:"CnpjDaIndustria"`
}

func (i *IdentificacaoDoArquivo) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIdentificacaoDoArquivo

	err = posicaoParaValor.ReturnByType(&i.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.IdentificadorDoArquivo, "IdentificadorDoArquivo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CnpjDoDitribuidor, "CnpjDoDitribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DataDoProcessamento, "DataDoProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.HoraDoProcessamento, "HoraDoProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CnpjDaIndustria, "CnpjDaIndustria")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIdentificacaoDoArquivo = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"IdentificadorDoArquivo":                      {2, 17, 0},
	"CnpjDoDitribuidor":                      {17, 32, 0},
	"DataDoProcessamento":                      {32, 40, 0},
	"HoraDoProcessamento":                      {40, 48, 0},
	"CnpjDaIndustria":                      {48, 63, 0},
}