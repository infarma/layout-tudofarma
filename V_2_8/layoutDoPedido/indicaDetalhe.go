package layoutDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type IndicaDetalhe struct {
	TipoDoRegistro     int64  	`json:"TipoDoRegistro"`
	NumeroDoPedido     string 	`json:"NumeroDoPedido"`
	CodigoDoProduto    int64  	`json:"CodigoDoProduto"`
	Quantidade         int64  	`json:"Quantidade"`
	Desconto           float64	`json:"Desconto"`
	Prazo              int64  	`json:"Prazo"`
	UtilizacaoDesconto string 	`json:"UtilizacaoDesconto"`
	UtilizacaoPrazo    string 	`json:"UtilizacaoPrazo"`
	DataDeVencimento   string 	`json:"DataDeVencimento"`
	NumeroDoLoteVC     string 	`json:"NumeroDoLoteVC"`
}

func (i *IndicaDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIndicaDetalhe

	err = posicaoParaValor.ReturnByType(&i.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Desconto, "Desconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Prazo, "Prazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UtilizacaoDesconto, "UtilizacaoDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UtilizacaoPrazo, "UtilizacaoPrazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DataDeVencimento, "DataDeVencimento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroDoLoteVC, "NumeroDoLoteVC")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIndicaDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"NumeroDoPedido":                      {2, 14, 0},
	"CodigoDoProduto":                      {14, 27, 0},
	"Quantidade":                      {27, 32, 0},
	"Desconto":                      {32, 40, 5},
	"Prazo":                      {40, 43, 0},
	"UtilizacaoDesconto":                      {43, 44, 0},
	"UtilizacaoPrazo":                      {44, 45, 0},
	"DataDeVencimento":                      {45, 53, 0},
	"NumeroDoLoteVC":                      {53, 63, 0},
}