package layoutDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	TipoDoRegistro       int64 	`json:"TipoDoRegistro"`
	NumeroDoPedido       string	`json:"NumeroDoPedido"`
	QuantidadeDeItens    int64 	`json:"QuantidadeDeItens"`
	QuantidadeDeUnidades int64 	`json:"QuantidadeDeUnidades"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeDeItens, "QuantidadeDeItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeDeUnidades, "QuantidadeDeUnidades")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"NumeroDoPedido":                      {2, 14, 0},
	"QuantidadeDeItens":                      {14, 19, 0},
	"QuantidadeDeUnidades":                      {19, 29, 0},
}