package layoutDeCancelamentoDeNotaFiscal

import (
	"bufio"
	"os"
)

type ArquivoNotaFiscal struct {
	Trailer Trailer `json:"Trailer"`
}

func GetStruct(fileHandle *os.File) (ArquivoNotaFiscal, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoNotaFiscal{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "1":
			err := arquivo.Trailer.ComposeStruct(fileScanner.Text())
			return arquivo, err
		}
	}

	return arquivo, err
}
