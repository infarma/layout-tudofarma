package layoutDeCancelamentoDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	TipoDoRegistro int64 	`json:"TipoDoRegistro"`
	NumeroDoPedido int64 	`json:"NumeroDoPedido"`
	Motivo         string	`json:"Motivo"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.Motivo, "Motivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"NumeroDoPedido":                      {2, 17, 0},
	"Motivo":                      {17, 67, 0},
}