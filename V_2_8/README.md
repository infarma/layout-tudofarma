# Arquivo de Pedido

    Identificação do arquivo: 
    
        gerador_de_layouts layoutDoPedido identificacaoDoArquivo TipoDoRegistro:int64:0:2 IdentificadorDoArquivo:string:2:17 CnpjDoDitribuidor:int64:17:32 DataDoProcessamento:int64:32:40 HoraDoProcessamento:int64:40:48 CnpjDaIndustria:int64:48:63 

    Cabecalho Do Pedido:
    
        gerador_de_layouts layoutDoPedido cabecalhoDoPedido TipoDoRegistro:int64:0:2 CodigoDoCliente:int64:2:17 NumeroDoPedido:string:17:29 DataDoPedido:int64:29:37 TipoDaCompra:string:37:38 TipoDoRetorno:string:38:39 ApontadorCondicaoComercial:int64:39:44 NumeroPedidoDoCliente:string:44:59
        
    Indica Detalhe:
    
        gerador_de_layouts layoutDoPedido indicaDetalhe TipoDoRegistro:int64:0:2 NumeroDoPedido:string:2:14 CodigoDoProduto:int64:14:27 Quantidade:int64:27:32 Desconto:float64:32:40:5 Prazo:int64:40:43 UtilizacaoDesconto:string:43:44 UtilizacaoPrazo:string:44:45 DataDeVencimento:string:45:53 NumeroDoLoteVC:string:53:63
        
    Trailer:
        
        gerador_de_layouts layoutDoPedido trailer TipoDoRegistro:int64:0:2 NumeroDoPedido:string:2:14 QuantidadeDeItens:int64:14:19 QuantidadeDeUnidades:int64:19:29
        
    Condição de Pagamento:
    
        gerador_de_layouts layoutDoPedido condicaoDePagamento TipoDoRegistro:int64:0:2 CodigoCondicaoDePagamento:int64:2:7 DescricaoPagamento:string:7:37 NumeroDeParcelas:int64:37:40 PorcentagemDeDescontoFinanceiro:float64:40:45:2 PrazoDePagamentoP1:int64:45:48 PrazoDePagamentoP2:int64:48:51 PrazoDePagamentoP3:int64:51:54 PrazoDePagamentoP4:int64:54:57 PrazoDePagamentoP5:int64:57:60 PrazoDePagamentoP6:int64:60:63 PorcentagemDePagamentoParcela1:float64:63:68:2 PorcentagemDePagamentoParcela2:float64:68:73:2 PorcentagemDePagamentoParcela3:float64:73:78:2 PorcentagemDePagamentoParcela4:float64:78:83:2 PorcentagemDePagamentoParcela5:float64:83:88:2 PorcentagemDePagamentoParcela6:float64:88:95:2
        
        
# Layout de Retorno do Pedido

    Identificação do arquivo: 
    
        gerador_de_layouts layoutDeRetornoDoPedido identificacaoDoArquivo TipoDoRegistro:int64:0:2 IdentificadorDoArquivo:string:2:17 CnpjDoDitribuidor:int64:17:32 DataDoProcessamento:int64:32:40 HoraDoProcessamento:int64:40:48
        
    Cabeçalho do Pedido:
    
        gerador_de_layouts layoutDeRetornoDoPedido cabecalhoDoPedido TipoDoRegistro:int64:0:2 CnpjDaFarmacia:int64:2:17 NumeroDoPedido:string:17:29 DataDoProcessamento:int64:29:37 HoraDoProcessamento:int64:37:45 NumeroDoPedido:int64:45:57 Motivo:int64:57:59
        
    Retorno dos Itens do Pedido:
    
        gerador_de_layouts layoutDeRetornoDoPedido retornoDosItensDoPedido TipoDoRegistro:int64:0:2 CodigoDoProduto:int64:2:15 NumeroDoPedido:string:15:27 CondicaoDePagamento:string:27:28 QuantidadeAtendida:int64:28:33 DescontoAplicado:float64:33:41:5 PrazoConcedido:int64:41:44 QuantidadeNaoAtendida:int64:44:49 CodigoDoMotivo:int64:49:51 DescricaoDoMotivo:string:51:101
        
    Trailer:
        
        gerador_de_layouts layoutDeRetornoDoPedido trailer TipoDoRegistro:int64:0:2 NumeroDoPedido:string:2:14 QuantidadeDeLinhas:int64:14:19 QuantidadeItensAtendidos:int64:19:24 QuantidadeDeItensNaoAtendida:int64:24:29
        
# Layout de Nota Fiscal

    Identificação do arquivo: 
    
        gerador_de_layouts layoutDeNotaFiscal identificacaoDoArquivo TipoDoRegistro:int64:0:2 IdentificadorDoArquivo:string:2:17 Cnpj:int64:17:32 DataDoProcessamento:int64:32:40 HoraDoProcessamento:int64:40:48
        
    Cabeçalho da Nota Fiscal:
    
        gerador_de_layouts layoutDeNotaFiscal cabecalhoDaNotaFiscal TipoDoRegistro:int64:0:2 CnpjDaFarmacia:int64:2:17 CnpjDoCdFilialDoDistribuidor:int64:17:32 NumeroDaNotaFiscal:int64:32:40 NumeroDoPedido0:string:40:52 DataDoProcessamento:int64:52:60 HoraDoProcessamento:int64:60:68 NumeroDoPedido1:int64:68:80 ChaveDanfe:string:80:124                 
                                           
    Itens da Nota Fiscal:
    
        gerador_de_layouts layoutDeNotaFiscal itensDaNotaFiscal TipoDoRegistro:int64:0:2 CnpjDoCdFilialDoDistribuidor:int64:2:17 NumeroDaNotaFiscal:int64:17:25 CodigoDoProduto:string:25:39 NumeroDoPedido:string:39:51 CondicaoDePagamento:string:51:52 QuantidadeAtendida:int64:52:57 DescontoAplicado:float64:57:65:5 PrazoConcedido:int64:65:68                 
    
    Trailer:
    
        gerador_de_layouts layoutDeNotaFiscal trailer TipoDoRegistro:int64:0:2 CnpjDoCdFilialDoDistribuidor:int64:2:17 NumeroDaNotaFiscal:int64:17:25 NumeroDoPedido:string:25:37 QuantidadeDeLinhasItensDaNota:int64:37:42 QuantidadeTotalDeItensNaNota:int64:42:47                 
        
# Layout de Cancelamento de Nota Fiscal

    Trailer:
    
        gerador_de_layouts layoutDeCancelamentoDeNotaFiscal trailer TipoDoRegistro:int64:0:2 NumeroDoPedido:int64:2:17 Motivo:string:17:67                
                 
                                                      