package V_2_8

import (
	"bitbucket.org/infarma/layout-tudofarma/V_2_8/layoutDoPedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (layoutDoPedido.ArquivoDePedido, error) {
	return layoutDoPedido.GetStruct(fileHandle)
}
