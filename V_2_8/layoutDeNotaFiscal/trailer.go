package layoutDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Trailer struct {
	TipoDoRegistro                int64 	`json:"TipoDoRegistro"`
	CnpjDoCdFilialDoDistribuidor  int64 	`json:"CnpjDoCdFilialDoDistribuidor"`
	NumeroDaNotaFiscal            int64 	`json:"NumeroDaNotaFiscal"`
	NumeroDoPedido                string	`json:"NumeroDoPedido"`
	QuantidadeDeLinhasItensDaNota int64 	`json:"QuantidadeDeLinhasItensDaNota"`
	QuantidadeTotalDeItensNaNota  int64 	`json:"QuantidadeTotalDeItensNaNota"`
}

func (t *Trailer) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesTrailer

	err = posicaoParaValor.ReturnByType(&t.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.CnpjDoCdFilialDoDistribuidor, "CnpjDoCdFilialDoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroDaNotaFiscal, "NumeroDaNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeDeLinhasItensDaNota, "QuantidadeDeLinhasItensDaNota")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&t.QuantidadeTotalDeItensNaNota, "QuantidadeTotalDeItensNaNota")
	if err != nil {
		return err
	}

	return err
}

var PosicoesTrailer = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"CnpjDoCdFilialDoDistribuidor":                      {2, 17, 0},
	"NumeroDaNotaFiscal":                      {17, 25, 0},
	"NumeroDoPedido":                      {25, 37, 0},
	"QuantidadeDeLinhasItensDaNota":                      {37, 42, 0},
	"QuantidadeTotalDeItensNaNota":                      {42, 47, 0},
}