package layoutDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type ItensDaNotaFiscal struct {
	TipoDoRegistro               int64  	`json:"TipoDoRegistro"`
	CnpjDoCdFilialDoDistribuidor int64  	`json:"CnpjDoCdFilialDoDistribuidor"`
	NumeroDaNotaFiscal           int64  	`json:"NumeroDaNotaFiscal"`
	CodigoDoProduto              string 	`json:"CodigoDoProduto"`
	NumeroDoPedido               string 	`json:"NumeroDoPedido"`
	CondicaoDePagamento          string 	`json:"CondicaoDePagamento"`
	QuantidadeAtendida           int64  	`json:"QuantidadeAtendida"`
	DescontoAplicado             float64	`json:"DescontoAplicado"`
	PrazoConcedido               int64  	`json:"PrazoConcedido"`
}

func (i *ItensDaNotaFiscal) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItensDaNotaFiscal

	err = posicaoParaValor.ReturnByType(&i.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CnpjDoCdFilialDoDistribuidor, "CnpjDoCdFilialDoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroDaNotaFiscal, "NumeroDaNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CondicaoDePagamento, "CondicaoDePagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.QuantidadeAtendida, "QuantidadeAtendida")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.DescontoAplicado, "DescontoAplicado")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrazoConcedido, "PrazoConcedido")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItensDaNotaFiscal = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"CnpjDoCdFilialDoDistribuidor":                      {2, 17, 0},
	"NumeroDaNotaFiscal":                      {17, 25, 0},
	"CodigoDoProduto":                      {25, 39, 0},
	"NumeroDoPedido":                      {39, 51, 0},
	"CondicaoDePagamento":                      {51, 52, 0},
	"QuantidadeAtendida":                      {52, 57, 0},
	"DescontoAplicado":                      {57, 65, 5},
	"PrazoConcedido":                      {65, 68, 0},
}