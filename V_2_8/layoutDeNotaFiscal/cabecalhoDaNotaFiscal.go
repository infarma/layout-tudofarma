package layoutDeNotaFiscal

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CabecalhoDaNotaFiscal struct {
	TipoDoRegistro               int64 	`json:"TipoDoRegistro"`
	CnpjDaFarmacia               int64 	`json:"CnpjDaFarmacia"`
	CnpjDoCdFilialDoDistribuidor int64 	`json:"CnpjDoCdFilialDoDistribuidor"`
	NumeroDaNotaFiscal           int64 	`json:"NumeroDaNotaFiscal"`
	NumeroDoPedido0              string	`json:"NumeroDoPedido0"`
	DataDoProcessamento          int64 	`json:"DataDoProcessamento"`
	HoraDoProcessamento          int64 	`json:"HoraDoProcessamento"`
	NumeroDoPedido1              int64 	`json:"NumeroDoPedido1"`
	ChaveDanfe                   string	`json:"ChaveDanfe"`
}

func (c *CabecalhoDaNotaFiscal) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalhoDaNotaFiscal

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjDaFarmacia, "CnpjDaFarmacia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjDoCdFilialDoDistribuidor, "CnpjDoCdFilialDoDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDaNotaFiscal, "NumeroDaNotaFiscal")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedido0, "NumeroDoPedido0")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataDoProcessamento, "DataDoProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraDoProcessamento, "HoraDoProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedido1, "NumeroDoPedido1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ChaveDanfe, "ChaveDanfe")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalhoDaNotaFiscal = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                      {0, 2, 0},
	"CnpjDaFarmacia":                      {2, 17, 0},
	"CnpjDoCdFilialDoDistribuidor":                      {17, 32, 0},
	"NumeroDaNotaFiscal":                      {32, 40, 0},
	"NumeroDoPedido0":                      {40, 52, 0},
	"DataDoProcessamento":                      {52, 60, 0},
	"HoraDoProcessamento":                      {60, 68, 0},
	"NumeroDoPedido1":                      {68, 80, 0},
	"ChaveDanfe":                      {80, 124, 0},
}