package layoutDeNotaFiscal

import (
	"bufio"
	"os"
)

type ArquivoNotaFiscal struct {
	IdentificacaoDoArquivo IdentificacaoDoArquivo `json:"IdentificacaoDoArquivo"`
	CabecalhoDaNotaFiscal  CabecalhoDaNotaFiscal  `json:"CabecalhoDaNotaFiscal"`
	ItensDaNotaFiscal      []ItensDaNotaFiscal    `json:"ItensDaNotaFiscal"`
	Trailer                Trailer                `json:"Trailer"`
}

func GetStruct(fileHandle *os.File) (ArquivoNotaFiscal, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoNotaFiscal{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "0":
			err := arquivo.IdentificacaoDoArquivo.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "1":
			err := arquivo.CabecalhoDaNotaFiscal.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "2":
			var registroTemp ItensDaNotaFiscal
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.ItensDaNotaFiscal = append(arquivo.ItensDaNotaFiscal, registroTemp)
			return arquivo, err
		case "3":
			err := arquivo.Trailer.ComposeStruct(fileScanner.Text())
			return arquivo, err
		}
	}

	return arquivo, err
}
