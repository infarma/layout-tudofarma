package layoutDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type IndicaDetalhe struct {
	TipoDoRegistro             int64   `json:"TipoDoRegistro"`
	NumeroDoPedido             string  `json:"NumeroDoPedido"`
	CodigoDoProduto            int64   `json:"CodigoDoProduto"`
	Quantidade                 int64   `json:"Quantidade"`
	Desconto                   float64 `json:"Desconto"`
	Prazo                      int64   `json:"Prazo"`
	UtilizacaoDesconto         string  `json:"UtilizacaoDesconto"`
	UtilizacaoPrazo            string  `json:"UtilizacaoPrazo"`
	ValorBaseDoProduto         float64 `json:"ValorBaseDoProduto"`
	ValorFinalSemImpostos      float64 `json:"ValorFinalSemImpostos"`
	ApontadorCondicaoComercial int64   `json:"ApontadorCondicaoComercial"`
}

func (i *IndicaDetalhe) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesIndicaDetalhe

	err = posicaoParaValor.ReturnByType(&i.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoDoProduto, "CodigoDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Desconto, "Desconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Prazo, "Prazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UtilizacaoDesconto, "UtilizacaoDesconto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.UtilizacaoPrazo, "UtilizacaoPrazo")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorBaseDoProduto, "ValorBaseDoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ValorFinalSemImpostos, "ValorFinalSemImpostos")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ApontadorCondicaoComercial, "ApontadorCondicaoComercial")
	if err != nil {
		return err
	}

	return err
}

var PosicoesIndicaDetalhe = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":             {0, 2, 0},
	"NumeroDoPedido":             {2, 14, 0},
	"CodigoDoProduto":            {14, 27, 0},
	"Quantidade":                 {27, 32, 0},
	"Desconto":                   {32, 37, 2},
	"Prazo":                      {37, 40, 0},
	"UtilizacaoDesconto":         {40, 41, 0},
	"UtilizacaoPrazo":            {41, 42, 0},
	"ValorBaseDoProduto":         {42, 50, 2},
	"ValorFinalSemImpostos":      {50, 58, 2},
	"ApontadorCondicaoComercial": {58, 67, 0},
}
