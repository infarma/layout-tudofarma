package layoutDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CondicaoDePagamento struct {
	TipoDoRegistro                  int64   `json:"TipoDoRegistro"`
	CodigoCondicaoDePagamento       int64   `json:"CodigoCondicaoDePagamento"`
	DescricaoPagamento              string  `json:"DescricaoPagamento"`
	NumeroDeParcelas                int64   `json:"NumeroDeParcelas"`
	PorcentagemDeDescontoFinanceiro float64 `json:"PorcentagemDeDescontoFinanceiro"`
	PrazoDePagamentoP1              int64   `json:"PrazoDePagamentoP1"`
	PrazoDePagamentoP2              int64   `json:"PrazoDePagamentoP2"`
	PrazoDePagamentoP3              int64   `json:"PrazoDePagamentoP3"`
	PrazoDePagamentoP4              int64   `json:"PrazoDePagamentoP4"`
	PrazoDePagamentoP5              int64   `json:"PrazoDePagamentoP5"`
	PrazoDePagamentoP6              int64   `json:"PrazoDePagamentoP6"`
	PorcentagemDePagamentoParcela1  float64 `json:"PorcentagemDePagamentoParcela1"`
	PorcentagemDePagamentoParcela2  float64 `json:"PorcentagemDePagamentoParcela2"`
	PorcentagemDePagamentoParcela3  float64 `json:"PorcentagemDePagamentoParcela3"`
	PorcentagemDePagamentoParcela4  float64 `json:"PorcentagemDePagamentoParcela4"`
	PorcentagemDePagamentoParcela5  float64 `json:"PorcentagemDePagamentoParcela5"`
	PorcentagemDePagamentoParcela6  float64 `json:"PorcentagemDePagamentoParcela6"`
}

func (c *CondicaoDePagamento) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCondicaoDePagamento

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCondicaoDePagamento, "CodigoCondicaoDePagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DescricaoPagamento, "DescricaoPagamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDeParcelas, "NumeroDeParcelas")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDeDescontoFinanceiro, "PorcentagemDeDescontoFinanceiro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoDePagamentoP1, "PrazoDePagamentoP1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoDePagamentoP2, "PrazoDePagamentoP2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoDePagamentoP3, "PrazoDePagamentoP3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoDePagamentoP4, "PrazoDePagamentoP4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoDePagamentoP5, "PrazoDePagamentoP5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PrazoDePagamentoP6, "PrazoDePagamentoP6")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDePagamentoParcela1, "PorcentagemDePagamentoParcela1")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDePagamentoParcela2, "PorcentagemDePagamentoParcela2")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDePagamentoParcela3, "PorcentagemDePagamentoParcela3")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDePagamentoParcela4, "PorcentagemDePagamentoParcela4")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDePagamentoParcela5, "PorcentagemDePagamentoParcela5")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.PorcentagemDePagamentoParcela6, "PorcentagemDePagamentoParcela6")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCondicaoDePagamento = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":                  {0, 2, 0},
	"CodigoCondicaoDePagamento":       {2, 7, 0},
	"DescricaoPagamento":              {7, 37, 0},
	"NumeroDeParcelas":                {37, 40, 0},
	"PorcentagemDeDescontoFinanceiro": {40, 45, 2},
	"PrazoDePagamentoP1":              {45, 48, 0},
	"PrazoDePagamentoP2":              {48, 51, 0},
	"PrazoDePagamentoP3":              {51, 54, 0},
	"PrazoDePagamentoP4":              {54, 57, 0},
	"PrazoDePagamentoP5":              {57, 60, 0},
	"PrazoDePagamentoP6":              {60, 63, 0},
	"PorcentagemDePagamentoParcela1":  {63, 68, 2},
	"PorcentagemDePagamentoParcela2":  {68, 73, 2},
	"PorcentagemDePagamentoParcela3":  {73, 78, 2},
	"PorcentagemDePagamentoParcela4":  {78, 83, 2},
	"PorcentagemDePagamentoParcela5":  {83, 88, 2},
	"PorcentagemDePagamentoParcela6":  {88, 95, 2},
}
