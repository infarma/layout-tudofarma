package layoutDoPedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	IdentificacaoDoArquivo IdentificacaoDoArquivo `json:"IdentificacaoDoArquivo"`
	CabecalhoDoPedido      CabecalhoDoPedido      `json:"CabecalhoDoPedido"`
	IndicaDetalhe          []IndicaDetalhe        `json:"IndicaDetalhe"`
	Trailer                Trailer                `json:"Trailer"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "0":
			err := arquivo.IdentificacaoDoArquivo.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "1":
			err := arquivo.CabecalhoDoPedido.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "2":
			var registroTemp IndicaDetalhe
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.IndicaDetalhe = append(arquivo.IndicaDetalhe, registroTemp)
			return arquivo, err
		case "3":
			err := arquivo.Trailer.ComposeStruct(fileScanner.Text())
			return arquivo, err
		}
	}
	return arquivo, err
}
