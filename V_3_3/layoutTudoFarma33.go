package V_3_3

import (
	"bitbucket.org/infarma/layout-tudofarma/V_3_3/layoutDoPedido"
	"os"
)

func GetArquivoDePedido(fileHandle *os.File) (layoutDoPedido.ArquivoDePedido, error) {
	return layoutDoPedido.GetStruct(fileHandle)
}
