# Arquivo de Pedido

    Identificação do arquivo: 
    
        gerador_de_layouts layoutDoPedido identificacaoDoArquivo TipoDoRegistro:int64:0:2 IdentificadorDoArquivo:string:2:17 CnpjDoDitribuidor:int64:17:32 DataDoProcessamento:int64:32:40 HoraDoProcessamento:int64:40:48 CnpjDaIndustria:int64:48:63 

    Cabecalho Do Pedido:
    
        gerador_de_layouts layoutDoPedido cabecalhoDoPedido TipoDoRegistro:int64:0:2 CodigoDoCliente:int64:2:17 NumeroDoPedido:string:17:29 DataDoPedido:int64:29:37 TipoDaCompra:string:37:38 TipoDoRetorno:string:38:39 ApontadorCondicaoComercial:int64:39:44 NumeroPedidoDoCliente:string:44:59
        
    Indica Detalhe:
    
        gerador_de_layouts layoutDoPedido indicaDetalhe TipoDoRegistro:int64:0:2 NumeroDoPedido:string:2:14 CodigoDoProduto:int64:14:27 Quantidade:int64:27:32 Desconto:float64:32:37:2 Prazo:int64:37:40 UtilizacaoDesconto:string:40:41 UtilizacaoPrazo:string:41:42 ValorBaseDoProduto:float64:42:50:2 ValorFinalSemImpostos:float64:50:58:2 ApontadorCondicaoComercial:int64:58:67
        
    Trailer:
        
        gerador_de_layouts layoutDoPedido trailer TipoDoRegistro:int64:0:2 NumeroDoPedido:string:2:14 QuantidadeDeItens:int64:14:19 QuantidadeDeUnidades:int64:19:29
        
        
# Layout de Retorno do Pedido

    Identificação do arquivo: 
    
        gerador_de_layouts layoutDeRetornoDoPedido identificacaoDoArquivo TipoDoRegistro:int64:0:2 IdentificadorDoArquivo:string:2:17 CnpjDoDitribuidor:int64:17:32 DataDoProcessamento:int64:32:40 HoraDoProcessamento:int64:40:48
        
    Cabeçalho do Pedido:
    
        gerador_de_layouts layoutDeRetornoDoPedido cabecalhoDoPedido TipoDoRegistro:int64:0:2 CnpjDaFarmacia:int64:2:17 NumeroDoPedido:string:17:29 DataDoProcessamento:int64:29:37 HoraDoProcessamento:int64:37:45 NumeroDoPedido:int64:45:57 Motivo:int64:57:59
        
    Retorno dos Itens do Pedido:
    
        gerador_de_layouts layoutDeRetornoDoPedido retornoDosItensDoPedido TipoDoRegistro:int64:0:2 CodigoDoProduto:int64:2:15 NumeroDoPedido:string:15:27 CondicaoDePagamento:string:27:28 QuantidadeAtendida:int64:28:33 DescontoAplicado:float64:33:38:2 PrazoConcedido:int64:38:41 QuantidadeNaoAtendida:int64:41:46 CodigoDoMotivo:int64:46:48 DescricaoDoMotivo:string:48:98 ValorBaseDoProdutoPfParaMedicamentos:float64:98:106:2 ValorFinalSemImpostos:float64:106:114:2 ApontadorCondicaoComrecial:int64:114:117
        
    Trailer:
        
        gerador_de_layouts layoutDeRetornoDoPedido trailer TipoDoRegistro:int64:0:2 NumeroDoPedido:string:2:14 QuantidadeDeLinhas:int64:14:19 QuantidadeItensAtendidos:int64:19:24 QuantidadeDeItensNaoAtendida:int64:24:29
        
# Layout de Nota Fiscal

    Identificação do arquivo: 
    
        gerador_de_layouts layoutDeNotaFiscal identificacaoDoArquivo TipoDoRegistro:int64:0:2 IdentificadorDoArquivo:string:2:17 Cnpj:int64:17:32 DataDoProcessamento:int64:32:40 HoraDoProcessamento:int64:40:48
        
    Cabeçalho da Nota Fiscal:
    
        gerador_de_layouts layoutDeNotaFiscal cabecalhoDaNotaFiscal TipoDoRegistro:int64:0:2 CnpjDaFarmacia:int64:2:17 CnpjDoCdFilialDoDistribuidor:int64:17:32 NumeroDaNotaFiscal:int64:32:40 NumeroDoPedido0:string:40:52 DataDoProcessamento:int64:52:60 HoraDoProcessamento:int64:60:68 NumeroDoPedido1:int64:68:80 ChaveDanfe:string:80:124                 
                                           
    Itens da Nota Fiscal:
    
        gerador_de_layouts layoutDeNotaFiscal itensDaNotaFiscal TipoDoRegistro:int64:0:2 CnpjDoCdFilialDoDistribuidor:int64:2:17 NumeroDaNotaFiscal:int64:17:25 CodigoDoProduto:string:25:39 NumeroDoPedido:string:39:51 CondicaoDePagamento:string:51:52 QuantidadeAtendida:int64:52:57 DescontoAplicado:float64:57:62:2 PrazoConcedido:int64:62:65                 
    
    Trailer:
    
        gerador_de_layouts layoutDeNotaFiscal trailer TipoDoRegistro:int64:0:2 CnpjDoCdFilialDoDistribuidor:int64:2:17 NumeroDaNotaFiscal:int64:17:25 NumeroDoPedido:string:25:37 QuantidadeDeLinhasItensDaNota:int64:37:42 QuantidadeTotalDeItensNaNota:int64:42:47                 
        
# Layout de Cancelamento de Nota Fiscal

    Trailer:
    
        gerador_de_layouts layoutDeCancelamentoDeNotaFiscal trailer TipoDoRegistro:int64:0:2 NumeroDoPedido:int64:2:17 Motivo:string:17:67                
                 
                                                      