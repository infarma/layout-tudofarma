package layoutDeRetornoDoPedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type CabecalhoDoPedido struct {
	TipoDoRegistro      int64  `json:"TipoDoRegistro"`
	CnpjDaFarmacia      int64  `json:"CnpjDaFarmacia"`
	NumeroDoPedido      string `json:"NumeroDoPedido"`
	DataDoProcessamento int64  `json:"DataDoProcessamento"`
	HoraDoProcessamento int64  `json:"HoraDoProcessamento"`
	NumeroDoPedido      int64  `json:"NumeroDoPedido"`
	Motivo              int64  `json:"Motivo"`
}

func (c *CabecalhoDoPedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalhoDoPedido

	err = posicaoParaValor.ReturnByType(&c.TipoDoRegistro, "TipoDoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjDaFarmacia, "CnpjDaFarmacia")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataDoProcessamento, "DataDoProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.HoraDoProcessamento, "HoraDoProcessamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroDoPedido, "NumeroDoPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.Motivo, "Motivo")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalhoDoPedido = map[string]gerador_layouts_posicoes.Posicao{
	"TipoDoRegistro":      {0, 2, 0},
	"CnpjDaFarmacia":      {2, 17, 0},
	"NumeroDoPedido":      {17, 29, 0},
	"DataDoProcessamento": {29, 37, 0},
	"HoraDoProcessamento": {37, 45, 0},
	"NumeroDoPedido":      {45, 57, 0},
	"Motivo":              {57, 59, 0},
}
