package layoutDeRetornoDoPedido

import (
	"bufio"
	"os"
)

type RetornoDoPedido struct {
	IdentificacaoDoArquivo  IdentificacaoDoArquivo    `json:"IdentificacaoDoArquivo"`
	CabecalhoDoPedido       CabecalhoDoPedido         `json:"CabecalhoDoPedido"`
	RetornoDosItensDoPedido []RetornoDosItensDoPedido `json:"RetornoDosItensDoPedido"`
	Trailer                 Trailer                   `json:"Trailer"`
}

func GetStruct(fileHandle *os.File) (RetornoDoPedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := RetornoDoPedido{}
	var err error
	for fileScanner.Scan() {
		runes := []rune(fileScanner.Text())
		identificador := string(runes[0:2])

		switch identificador {
		case "0":
			err := arquivo.IdentificacaoDoArquivo.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "1":
			err := arquivo.CabecalhoDoPedido.ComposeStruct(fileScanner.Text())
			return arquivo, err
		case "2":
			var registroTemp RetornoDosItensDoPedido
			err = registroTemp.ComposeStruct(string(runes))
			arquivo.RetornoDosItensDoPedido = append(arquivo.RetornoDosItensDoPedido, registroTemp)
			return arquivo, err
		case "3":
			err := arquivo.Trailer.ComposeStruct(fileScanner.Text())
			return arquivo, err
		}
	}

	return arquivo, err
}
